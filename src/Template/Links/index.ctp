<h1>Enlaces disponibles</h1>

<?= $this->Html->link('Añadir enlace', ['action' => 'add'])?>

<table>
	<tr>
		<th>Id</th>
		<th>Title</th>
		<th>URL</th>
		<th>Created</th>
	</tr>

	<?php foreach ($links as $link): ?>
	<tr>
		<td>
			<?= $link->id ?>
		</td>
		<td>
			<?= $this->Html->link($link->title, ['action' => 'view', $link->id]) ?>
		</td>
		<td>
			<?= $link->url ?>
		</td>
		<td>
			<?= $link->created->format(DATE_RFC850) ?>
		</td>
	</tr>
	 
	
	





	<?php endforeach; ?>
</table>
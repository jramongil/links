<?php 

namespace App\Controller;

use Cake\Controller\Controller;


class LinksController extends AppController {
 
    var $name = 'Links';
 
    function add() {
        $this->Links->newEntity();
        if (!empty($this->data)) {
            if ($this->Links->save($this->data)) {
                $this->flash('Enlace añadido.','/links');
            }
        }
    }
 
    function index() {
        $this->set('links', $this->Links->find('all'));
    }
}




?>